import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "@/assets/scss/app.scss";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import "./assets/tailwind.css";

const app = createApp(App);

app.use(router);
app.component("FontAwesomeIcon", FontAwesomeIcon);
app.mount("#app");
