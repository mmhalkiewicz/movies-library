import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../views/HomePage";
import MovieDetails from "@/views/MovieDetails";
import tvShowDetails from "@/views/SerialDetails";

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomePage,
  },
  {
    path: "/movie/:id",
    name: "movie",
    component: MovieDetails,
    meta: { NavigationComponent: false },
    props: true,
  },

  {
    path: "/tv-show/:id",
    name: "tv-show",
    component: tvShowDetails,
    meta: { NavigationComponent: false },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
